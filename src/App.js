import './App.css';
import React from "react";
import ComponentName from './ComponentName';

function App() {
  return (
    <div className="App">
    
    <ComponentName country="Finland!"/>
    <ComponentName country="Norway!"/>
    <ComponentName country="Sweden!"/>
    </div>
  );
}
export default App;
